# Copyright 2010 Wulf C. Krueger <philantrop@exherbo.org>
# Copyright 2013 Elias Pipping <pipping@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require elisp-module [ pn=bbdb ]

if ever is_scm; then
    SCM_REPOSITORY="https://git.savannah.gnu.org/git/${PN}.git"
    require scm-git autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 ] ]
else
    DOWNLOADS="mirror://savannah/${PN}/${PNV}.tar.gz"
fi

SUMMARY="Insidious Big Brother Database for GNU Emacs"
DESCRIPTION="
The Insidious Big Brother DataBase is an emacs-based contact manager that integrates
itself into your mail and news clients.
"
HOMEPAGE="https://savannah.nongnu.org/projects/${PN}"

LICENCES="|| ( GPL-2 GPL-3 )"
SLOT="0"
MYOPTIONS=""

# requires TeX
RESTRICT="test"

DEPENDENCIES="
    build+run:
        app-editors/emacs
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --with-lispdir="${ELISP_SITE_LISP}"/${PN}
)

src_compile() {
    emake -C lisp
}

src_install() {
    emake -C lisp DESTDIR="${IMAGE}" install

    elisp-install ${PN} lisp/*.el{,c}
    elisp-install-site-file

    ! ever is_scm && doinfo doc/*.info*
}

